
    var exec = require('cordova/exec');

    var PLUGIN_NAME = 'GetLocationMueve';

    var FakeLocationMueve = {
        check: function (successCallback, errorCallback) {
            exec(successCallback, errorCallback, PLUGIN_NAME, 'check', []);
        }
    };

    module.exports = FakeLocationMueve;
